#include "easysound.h"

EasySound::EasySound(void)
{
  
    // Initialize the environment
    alutInit(0, NULL);

    // Capture errors
    alGetError();
}

bool EasySound::addSound(std::string filename, bool loop)
{
    ALuint buffer = alutCreateBufferFromFile(filename.c_str());
    ALuint source;
    //Load pcm data into buffer
    alGenSources(1, &source);
    alSourcei(source, AL_BUFFER, buffer);
    
    // Create sound source (use buffer to fill source)
    alGenSources(1, &source);
    alSourcei(source, AL_BUFFER, buffer);
    
    if(loop)
      alSourcei (source, AL_LOOPING,  AL_TRUE  );
    
    buffers.push_back(buffer);
    sources.push_back(source);
}

bool EasySound::play(int id)
{
  alSourcePlay(sources[id]);
}

bool EasySound::stop(int id)
{
  alSourceStop(sources[id]);
}

bool EasySound::pitch(int id, double pitch)
{
  alSourcef(sources[id],AL_PITCH, pitch);
}


bool EasySound::pitchUp(int id)
{
  float pitch;
  alGetSourcef(sources[id], AL_PITCH, &pitch);
  alSourcef(sources[id],AL_PITCH, pitch+0.05);
}

bool EasySound::pitchDown(int id)
{
  float pitch;
  alGetSourcef(sources[id], AL_PITCH, &pitch);
  alSourcef(sources[id],AL_PITCH, pitch-0.05);
}
bool EasySound::setVol(int id, float vol)
{
  alSourcef(sources[id],AL_GAIN, vol);
}

bool EasySound::volUp(int id)
{
  float gain;
  alGetSourcef(sources[id], AL_GAIN, &gain);
  alSourcef(sources[id],AL_GAIN, gain+0.05);
}

bool EasySound::volDown(int id)
{
  float gain;
  alGetSourcef(sources[id], AL_GAIN, &gain);
  alSourcef(sources[id],AL_GAIN, gain-0.05);
}

bool EasySound::playAll()
{
  for(int i=0; i<sources.size();i++)
    alSourcePlay(sources[i]);
    
}

bool EasySound::stopAll()
{
  for(int i=0; i<sources.size();i++)
    alSourceStop(sources[i]);
}

EasySound::~EasySound(void)
{
  for(int i=0; i<sources.size();i++) 
    alDeleteSources(1, &sources[i]);
  for(int i=0; i<buffers.size();i++) 
    alDeleteBuffers(1, &buffers[i]);
}