//***  Autor: Ramón Pérez Pérez
//***  Practica final del Curso de Realidad aumentada
//***  Incorpora:
//***  	- Estabilizado de las marcas
//***   - Objetos de Blender (no termina de mostrar los objetos)
//***   - Sonido con distancia/volumen y rotacionX/pitch
//***   

#include <math.h>
#include <GL/glut.h>    
#include <AR/gsub.h>    
#include <AR/video.h>   
#include <AR/param.h>   
#include <AR/ar.h>
#include <string>
#include <vector>

#include "orej.h"
#include "easysound.h"

// ==== Definicion de estructuras ===================================
struct TObject{
  int id;                      // Identificador del patron
  int visible;                 // Es visible el objeto?
  double width;                // Ancho del patron
  double center[2];            // Centro del patron  
  double patt_trans[3][4];     // Matriz asociada al patron
  int sounding;	       // Controla si ya está activo, para que no reinicie el sonido
  void (* drawme)(void);       // Puntero a funcion drawme
  double init_angle;
  double init_distance;
  double distance;
  double angle;
  OrjObjeto orej;
};

struct TObject *objects = NULL;
int nobjects = 0;
EasySound *mplayer = NULL;

int    useCont = 0;        // Inicialmente no puede usar estabilizado
int    contAct = 0;        // Indica si queremos usar el estabilizado

void print_error (char *error) {  printf("%s\n", error); exit(0); }

// ==== addObject (Anade objeto a la lista de objetos) ==============

// ** anade objeto de Blender
void addObject(char *p, char *model_orj, char *model_ppm, double w, double c[2]){
  int pattid;

  if((pattid=arLoadPatt(p)) < 0) 
    print_error ("Error en carga de patron\n");

  nobjects++;
  objects = (struct TObject *) 
    realloc(objects, sizeof(struct TObject)*nobjects);

  objects[nobjects-1].id = pattid;
  objects[nobjects-1].width = w;
  objects[nobjects-1].center[0] = c[0];
  objects[nobjects-1].center[1] = c[1];
  objects[nobjects-1].drawme = NULL;
  objects[nobjects-1].sounding = 0;

  cargarObjeto(&objects[nobjects-1].orej, model_orj, model_ppm, NOANIM, 190);

}

// ** anade objeto normal de Glut
void addObject(char *p, double w, double c[2], void (*drawme)(void)) 
{
  int pattid;

  if((pattid=arLoadPatt(p)) < 0) 
    print_error ("Error en carga de patron\n");

  nobjects++;
  objects = (struct TObject *) 
    realloc(objects, sizeof(struct TObject)*nobjects);

  objects[nobjects-1].id = pattid;
  objects[nobjects-1].width = w;
  objects[nobjects-1].center[0] = c[0];
  objects[nobjects-1].center[1] = c[1];
  objects[nobjects-1].drawme = drawme;
  objects[nobjects-1].sounding = 0;

}

// ==== addSound (Anade objeto a la lista de objetos) ==============
void addSound(char *p) 
{

  if(mplayer == NULL) 
	mplayer = new EasySound();

  mplayer->addSound(p, true);

}


// ==== draw****** (Dibujado especifico de cada objeto) =============

void drawteapot(void) {
  GLfloat material[]     = {0.0, 0.0, 1.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glTranslatef(0.0, 0.0, 60.0);
  glRotatef(90.0, 1.0, 0.0, 0.0);
  glutSolidTeapot(80.0);
}

void drawcube(void) {
  GLfloat material[]     = {1.0, 0.0, 0.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glTranslatef(0.0, 0.0, 40.0);
  glutSolidCube(80.0);
}

void drawearth(void) {
  GLfloat material[]     = {0.1, 0.1, 1.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glutSolidSphere (20, 16, 16); // Tierra
}

void drawsun(void) {
  GLfloat material[]     = {1.0, 0.9, 0.0, 1.0};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material);
  glTranslatef(0,0,80);
  glutSolidSphere (80, 16, 8); // Sol	
}

// ======== cleanup =================================================
static void cleanup(void) {   // Libera recursos al salir ...
  arVideoCapStop();  arVideoClose();  argCleanup();  free(objects);  
  exit(0);
}

// ======== keyboard ================================================
static void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 'E': case 'e':
    if (contAct) {contAct = 0; printf("Desactivado el uso de estabilizado\n");}
    else {contAct = 1; printf("Activado el uso de esabilizado\n");} break;
  case 0x1B: case 'Q': case 'q':
    cleanup(); break;
  }
}

// ======== draw ====================================================
void draw( void ) {
  double  gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
  GLfloat light_position[]  = {100.0,-200.0,200.0,0.0};
  int i;
  
  argDrawMode3D();              // Cambiamos el contexto a 3D
  argDraw3dCamera(0, 0);        // Y la vista de la camara a 3D
  glClear(GL_DEPTH_BUFFER_BIT); // Limpiamos buffer de profundidad
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  for (i=0; i<nobjects; i++) {
    if (objects[i].visible) {   // Si el objeto es visible
      argConvGlpara(objects[i].patt_trans, gl_para);   
      glMatrixMode(GL_MODELVIEW);           
      glLoadMatrixd(gl_para);   // Cargamos su matriz de transf.            

      if (objects[i].drawme != NULL){

	glEnable(GL_LIGHTING);  glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	objects[i].drawme();      // Llamamos a su función de dibujar

      }else{

//	printf("Desplegando objeto %d\n", i);
	desplegarObjeto(&objects[i].orej, EWIREFRAME);

      }
    }
  }

  glDisable(GL_DEPTH_TEST);
}

// ======== sound ====================================================
void sound( void ) {
  double  gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
  int i;
  double vol;
  double change_angle;
  
  for (i=0; i<nobjects; i++) {
    if (objects[i].visible) {   // Si el objeto es visible
	// activamos musica para ese objeto
	if (objects[i].sounding == 0){
		mplayer->play(i);
		objects[i].sounding = 1;
	}

	// distancia real entre 700 y 200, en un rango de 0.1 a 1. a = -0.9/500.0, b = 1 - 200a = 1.36
	vol = ((-0.9/500.0) * objects[i].distance) + 1.36;
	if (vol < 0) vol = 0; if (vol > 1) vol = 1;	
	mplayer->setVol(i, vol);

	// pitch
	change_angle = objects[i].angle / objects[i].init_angle;
//	printf("Cambio %G\n", change_angle);
	if (change_angle > 1.1) {
		mplayer->pitchUp(i);
		objects[i].init_angle = objects[i].angle;
	}
	if (change_angle < 0.9){
		mplayer->pitchDown(i);
		objects[i].init_angle = objects[i].angle;
	}

    }else{
	mplayer->stop(i);
	mplayer->setVol(i, 0);
	mplayer->pitch(i, 1);
	objects[i].sounding = 0;
    }
  }
}


// ======== init ====================================================
static void init( void ) {
  ARParam  wparam, cparam;   // Parametros intrinsecos de la camara
  int xsize, ysize;          // Tamano del video de camara (pixels)
  double c[2] = {0.0, 0.0};  // Centro de patron (por defecto)
  
  // Abrimos dispositivo de video
  if(arVideoOpen("") < 0) exit(0);  
  if(arVideoInqSize(&xsize, &ysize) < 0) exit(0);

  // Cargamos los parametros intrinsecos de la camara
  if(arParamLoad("data/camera_para.dat", 1, &wparam) < 0)   
    print_error ("Error en carga de parametros de camara compruebe que existe el fichero data/camera_para.dat\n");
  
  arParamChangeSize(&wparam, xsize, ysize, &cparam);
  arInitCparam(&cparam);   // Inicializamos la camara con "cparam"

  // Inicializamos la lista de objetos
  addObject("data/patron.patt", 120.0, c, drawteapot); 
  addObject("data/patron2.patt", "modelos_orj/oveja.orj", "modelos_orj/oveja.ppm", 90.0, c);
  addObject("data/patron3.patt", 90.0, c, drawearth);
  addObject("data/patron4.patt", 90.0, c, drawcube); 

  // Inicializamos la lista de musica
  addSound( "sound/sound1.wav");
  addSound( "sound/sound2.wav");
  addSound( "sound/sound3.wav");
  addSound( "sound/sound4.wav");

  argInit(&cparam, 1.0, 0, 0, 0, 0);   // Abrimos la ventana 
}

void getDistance(int i){
	
    objects[i].distance = sqrt(pow(objects[i].patt_trans[0][3],2)+pow(objects[i].patt_trans[1][3],2)+pow(objects[i].patt_trans[2][3],2));
//    printf ("Distancia %d --> %G\n", i, objects[i].distance);

}

void getAngle(int i){
	
    objects[i].angle = objects[i].patt_trans[1][0];
//    printf ("Angulo %d --> %G\n", i, objects[i].angle);

}

void setVisible(int i){

    getDistance(i);  		// distance to camera
    getAngle(i);		// get rotation

    if (objects[i].visible == 0){
   	objects[i].visible = 1; 			// visible ON
	objects[i].init_distance = objects[i].distance; // set init distance to relative vol
	objects[i].init_angle = objects[i].angle;
    }

}

// ======== mainLoop ================================================
static void mainLoop(void) {
  ARUint8 *dataPtr;
  ARMarkerInfo *marker_info;
  int marker_num, i, j, k;

  // Capturamos un frame de la camara de video
  if((dataPtr = (ARUint8 *)arVideoGetImage()) == NULL) {
    // Si devuelve NULL es porque no hay un nuevo frame listo
    arUtilSleep(2);  return;  // Dormimos el hilo 2ms y salimos
  }

  argDrawMode2D();
  argDispImage(dataPtr, 0,0);    // Dibujamos lo que ve la camara 

  // Detectamos la marca en el frame capturado (return -1 si error)
  if(arDetectMarker(dataPtr, 100, &marker_info, &marker_num) < 0) {
    cleanup(); exit(0);   // Si devolvio -1, salimos del programa!
  }

  arVideoCapNext();      // Frame pintado y analizado... A por otro!

  // Vemos donde detecta el patron con mayor fiabilidad
  for (i=0; i<nobjects; i++) {
    for(j = 0, k = -1; j < marker_num; j++) {
      if(objects[i].id == marker_info[j].id) {
	if (k == -1) k = j;
	else if(marker_info[k].cf < marker_info[j].cf) k = j;
      }
    }
    
    if(k != -1) {   // Si ha detectado el patron en algun sitio...

	    // Obtener transformacion relativa entre marca y la camara real
	    if (useCont && contAct) {
	      arGetTransMatCont(&marker_info[k], objects[i].patt_trans, objects[i].center, objects[i].width, objects[i].patt_trans);
	      //printf ("Usando estabilizado de patron\n");
	    }
	    else {
	      useCont = 1;  // En la siguiente iteracion lo podemos usar!
	      arGetTransMat(&marker_info[k], objects[i].center, objects[i].width, objects[i].patt_trans);
	      //printf ("Sin estabilizado de patrón\n");
	    }

      setVisible(i); // active object and get distance to camera

    } else { objects[i].visible = 0; }  // El objeto no es visible
  }
 
  draw();           // Dibujamos los objetos de la escena
  sound();
  argSwapBuffers(); // Cambiamos el buffer con lo que tenga dibujado

}



// ======== Main ====================================================
int main(int argc, char **argv) {

  glutInit(&argc, argv);    // Creamos la ventana OpenGL con Glut
  init();                   // Llamada a nuestra funcion de inicio
  
  arVideoCapStart();        // Creamos un hilo para captura de video
  argMainLoop( NULL, keyboard, mainLoop );    // Asociamos callbacks

  return (0);
}
