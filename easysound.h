#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <iostream>

class EasySound
{
	std::vector<ALuint> buffers;
	std::vector<ALuint> sources;
	ALuint state;
	
	
public:

	EasySound(void);
	
	//Add sound to list of sounds
	bool addSound(std::string filename, bool loop);
	
	//Play and stop by id
	bool play(int id);
	bool stop(int id);
	
	//Play and stop all together
	bool playAll();
	bool stopAll();
	
	//Pitch management
	bool pitch(int id, double pitch);
	bool pitchUp(int id);
	bool pitchDown(int id);
	
	//Gain management
	bool volUp(int id);
	bool volDown(int id);
	bool setVol(int id, float vol);
	
	~EasySound(void);

};
